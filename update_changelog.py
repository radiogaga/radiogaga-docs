import os

def UpdateChangelog():
  os.popen('svn update')
  lines = os.popen('svn log').readlines()
  #print(lines)

  with open("docs/changelog.md",'w') as f:
    f.write("# Changelog\n\n")
    for line in lines:
      # Supression des lignes de svn log
      if line == "------------------------------------------------------------------------\n": 
        line = ""
      # Entete du commit
      elif line[0] == 'r' and line[1].isnumeric(): 
        line = line.replace("\n","")  # supression du saut de ligne
        line = line.split("|")
        print(line)
        date =line[2].split("(")[1]
        print(date)
        line = '!!! info "'+ line[0] + " | " + line[1] + " | " + date[0:-2] + '"\n'

      else:
        line = "    " + line
      f.write(line)

UpdateChangelog()


