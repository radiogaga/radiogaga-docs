# Bienvenue sur la documentation de RadioGagA

![](figures/first_page.jpg)

RadioGaGA est un récepteur développé sur l'instrument NenuFar. Il consiste à détecter sur des formes d'ondes des événements déclenchés par des gerbes atmosphériques de rayons Gammas. 
Ce projet tourne sur des cartes du backend de test. 

Le détecteur a été développé en vhdl sur les cartes HPAPB. 2 cartes sont actuellement en fonctionnement soit 8MR.   
Le principe du firmware est, sur une somme phasé et filtré de tous les MR, venir trigger sur des événements très impulsif et rapide afin de déclancher les TBB pour extraire la forme d'onde sur l'ensemble des MR de l'instrument. 

