# Changelog

!!! info "r10386  |  berthet  | Mon 27 Jun 2022"
    
    - Update TBB dump cheat sheet with new rsync command
    - Add tests : monitor, dirac_injection, inversion_polar, test_latence
!!! info "r10126  |  berthet  | Thu 20 Jan 2022"
    
    Update cheat sheet
!!! info "r10125  |  berthet  | Thu 20 Jan 2022"
    
    - Test report on synchro test for TBB dump
    - Add cheat sheet in doc for dump's commands
!!! info "r10110  |  berthet  | Thu 13 Jan 2022"
    
    Add plot_spectrogram.py doc
!!! info "r10051  |  berthet  | Fri 10 Dec 2021"
    
    Remove site and pdf directories
!!! info "r10050  |  berthet  | Fri 10 Dec 2021"
    
    - Add TBB burst/ CygA transit / adress register / installation Doc
    - Modify Configuration_files/bootsequence/Home Doc
    - Replace changelog.sh by update_changelog.py
!!! info "r10049  |  berthet  | Fri 10 Dec 2021"
    
    - Remove generated html in site/ - Replace by DOCUMENTATION.html
    - Remove Dockerfile - Replace by requirements.txt
!!! info "r9979  |  berthet  | Wed 13 Oct 2021"
    
    - Add tests reports section
    - Move old home page to installation section
    - Change home page
!!! info "r9937  |  berthet  | Fri 17 Sep 2021"
    
    Try to add radiogaga doc in .html
!!! info "r9852  |  berthet  | Mon 28 Jun 2021"
    
    Add pdf
!!! info "r9851  |  berthet  | Mon 28 Jun 2021"
    
    - Add Firmware/hierarchy section  
    - Add Divers/Pyfda section  
    - Set up navigation bar for easier navigation between sections  
    - Remove changelog.md from versioning, auto-generated
!!! info "r9839  |  berthet  | Tue 22 Jun 2021"
    
    Fix bug with name of pdf, finish Configuration_files page, update udp_sender register table.
!!! info "r9827  |  berthet  | Fri 18 Jun 2021"
    
    Add pdf
!!! info "r9826  |  berthet  | Fri 18 Jun 2021"
    
    Update software section
!!! info "r9825  |  berthet  | Fri 18 Jun 2021"
    
    Add todo list plugin
!!! info "r9824  |  berthet  | Fri 18 Jun 2021"
    
    remove sudo in script, add procedure to add docker to user group in Index.md
!!! info "r9823  |  berthet  | Fri 18 Jun 2021"
    
    modify dockerfile, use a very lighter version of python
!!! info "r9817  |  viou  | Thu 17 Jun 2021"
    
    Move changelog at the end of the doc and include all doc files (incl. scripts) in changelog
!!! info "r9816  |  viou  | Thu 17 Jun 2021"
    
    boot -> startup
!!! info "r9815  |  berthet  | Thu 17 Jun 2021"
    
    doc - Update generation scripts
!!! info "r9814  |  berthet  | Thu 17 Jun 2021"
    
    doc - Add pdf
!!! info "r9813  |  berthet  | Thu 17 Jun 2021"
    
    doc - Move css and js in assets dir
!!! info "r9812  |  berthet  | Thu 17 Jun 2021"
    
    doc - Add subdirectory to better structure the doc dir.
!!! info "r9803  |  berthet  | Mon 14 Jun 2021"
    
    Update svn keywords in .yml
!!! info "r9802  |  berthet  | Mon 14 Jun 2021"
    
    Add pdf directory with first pdf
!!! info "r9801  |  berthet  | Mon 14 Jun 2021"
    
    New doc, generated with mkdoc.
!!! info "r9800  |  berthet  | Mon 14 Jun 2021"
    
    Remove old doc using makefile with pandoc
!!! info "r9758  |  berthet  | Wed 02 Jun 2021"
    
    Add documentation of IPD,DCB,Presum, Demande_ALSE_sumX
!!! info "r9757  |  berthet  | Wed 02 Jun 2021"
    
    DOCUMENTAION
    Update makefile, add some configurations files (template, script to install dependencies,font)
!!! info "r9640  |  berthet  | Thu 08 Apr 2021"
    
    Add directory RadioGAGA in receivers. This include docs and symbolic link to firmware and soft.
