# Splitter & cable

Pour connecter des MR au backend RadioGaGA on utilise des splitter.

On voulait faire un câble rajoutant 5ns de délai, soit 1 échantillon, donc :   

- Temps ajouté par le splitter : 0.7ns  
- Temps ajouté par le cable de 90cm : 4.3ns   
