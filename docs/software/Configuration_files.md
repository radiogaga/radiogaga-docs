# Configuration files for RadioGaga

Path : [svn/NenuFar/trunk/receivers/RadioGAGA/software/configuration_files/](https://svn.obs-nancay.fr/svn/NenuFar/trunk/receivers/RadioGAGA/configs/configuration_files/)

## Presentation des fichiers
Il y a 2 types de fichiers de configuration pour RadioGaGA. 

- startup_configuration_board_**n**.conf
- recursive_configuration_board_**n**.conf

!!! warning
     Il y'a un fichiers de chaque par carte


### **startup_configuration_board_X.conf** 

Contient une configuration stable au démarrage du service

### **recursive_configuration_board_X.conf**

Permet de modifiers des paramètres en cours de manip'. Tels que les paramètres du trigger ou encore les coefficients des filtres.
Ces fichiers sont générés à chaque 10s d'une minute.  
(exemple : 10:05:10, 12:31:10...)  

## Génération des fichiers
### Utilisation manuelle
Pour lancer ce script il faut avoir une working copy du depository [svn/NenuFar/trunk/recepteur_LANewBa/trunk/firmware/HPAPB/](https://svn.obs-nancay.fr/svn/NenuFar/trunk/recepteur_LANewBa/trunk/firmware/HPAPB/)

Et d'avoir configurer la variable d'environnement HPAPB_ROOT_DIR.

Exemple :   
```
export HPAPB_ROOT_DIR="/home/berthet/Nancay/Nenufar/HPAPB/"
```
Par exemple : 

Ensuite il faut configuré les bons paramètres dans les fichiers yaml
=== "startup_configuration.yaml"
     ``` yaml
     # Input file to generate startup configurations files.
     master_board : 0
     nof_boards : 2
     # located in HPAPB/src/PROCESSING_MODULES/filter/src/data/PyFDA/
     filter: 
     - 0:
     path: pass_through.conf
     shift: 0
     - 1:
     path: pass_through.conf
     shift: 0
     - 2:
     path: pass_through.conf
     shift: 0

     trigger:
     k               : 200
     c_num           : 3
     c_den           : 2
     rising_time_min : 2
     rising_time_max : 10
     
     
     udp_sender:
     # TBB ip configuration
     mac: ff:ff:ff:ff:ff:ff
     ip: 192.168.7.128
     port: 1242
     delay: 64
     enable: 1

     # 1 enable - 0 disable
     presum:
     Board_0:
          Channel_0: 1
          Channel_1: 1
          Channel_2: 1
          Channel_3: 1
     Board_1:
          Channel_0: 1
          Channel_1: 1
          Channel_2: 1
          Channel_3: 1
     ```
=== "recursive_configuration.yaml"
     ``` yaml
     # Input file to generate recursive configurations files between 7h and 22h.

     master_board : 0
     nof_boards : 2

     # located in HPAPB/src/PROCESSING_MODULES/filter/src/data/PyFDA/
     filter: 
     - 0:
     path: band_24-76.conf
     shift: 15
     - 1:
     path: rej_36.conf
     shift: 15
     - 2:
     path: rej_73.conf
     shift: 15

     trigger:
     k               : 2000
     ```

Enfin il faut lancer le script **gen_configuration.py** file avec les arguments suivant :

  - fichier yaml souhaité 
  - -r pour exporter une configuration recursive
  - ou
  - -s pour exporter une configuration de démarrage

Exemple :  
```
python3 gen_configuration_file.py -s input/startup_configuration.yaml
```

Cela va générer les fichiers de sortie dans output/startup_configuration_board_**n**.conf avec **n** s'incrémentant pour chaque carte.  
Les fichiers sont générés dans le dossier **output/**.

### Mode jour/nuit
L'idée d'avoir une configuration de jour et de nuit semblait intéressante, principalement pour les filtres. L'idée étant d'appliqué des filtres moins violant étant donnée qu'il y'a moins de parasites.

Pour cela une tâche Cron (planificateur de tâches sous linux) pourrait lancer le script à 22h avec *input/night_configuration.yaml* et à 7H avec *input/day_configuration.yaml*


=== "day_configuration.yaml"
     ``` yaml
     # Input file to generate recursive configurations files between 7h and 22h.


     master_board : 0
     nof_boards : 2

     # located in HPAPB/src/PROCESSING_MODULES/filter/src/data/PyFDA/
     filter: 
      - 0:
        path: band_24-76.conf
        shift: 15
      - 1:
        path: rej_36.conf
        shift: 15
      - 2:
        path: rej_73.conf
        shift: 15
     ```
=== "recursive_configuration.yaml"
     ``` yaml
     # Input file to generate recursive configurations files between 7h and 22h.


     master_board : 0
     nof_boards : 2

     # located in HPAPB/src/PROCESSING_MODULES/filter/src/data/PyFDA/
     filter: 
      - 0:
        path: band_24-76.conf
        shift: 15
      - 1:
        path: pass_through.conf
        shift: 15
      - 2:
        path: pass_through.conf
        shift: 15
     ```
