# Database pour les événements TBB

## Introduction
**RadioGaGa** détecte de plus en plus d'événements au fil des nouvelles observations.  
Pour trier,afficher, traiter, tous ces événements ont été logger dans une base **InfluxDB**.  


L'avantage de cette base est sa temporalité, ce n'est pas une base SQL classique avec entrée/valeur.
Mais chaque ligne est associé à un **instant** avec ses différents **champs**(fields) associés.  
C'est un gros point fort pour enregistrer les événements à leur moment de déclenchement.


## Format d'un événement dans la base 

Pour écrire cela se fait lors du traitement des analyse via le [pipeline](https://gitlab.obspm.fr/radiogaga/radiogaga-python/-/blob/main/scripts/eparse/pipeline.py) tournant sur **codadqsa2**.

```python
#################
# Database insert
#################
if args.db:
starttime= time.time()#$$$
tags = {"source": f'{parset.ObsTopic.split(" ")[0]}/{parset.ObsName}',
        "polar_trigged": polar_trigged,
        "FPGA_ver":infos.FPGA_ver,
        "MR_used":ma_list,
        "is_ana_pointing":is_ana_pointing
    }

fields = {
    "path":args.path,
    "name":name,
    "udp_count":infos.cnt,
    "rising_time": float(processed.rising_time[polar_trigged] ),
    "max": float(processed.max[polar_trigged]),
    "noise_level": float(processed.level_bruit[polar_trigged]),
    "SN_x":float(radiogaga_mr_trig.SN[0]),
    "SN_y":float(radiogaga_mr_trig.SN[1]),
    "alt_source": float(alt_source),
    "az_source": float(az_source),
    'alt_event' : float(alt),
    'az_event' : float(az),
    'pos_x' : float(x),
    'pos_y' : float(y),
    'heatmap_ratio' : float(heatmap_ratio),
    'heatmap_coherent': bool(heatamp_coherent)
    }
db.insert_event(time_event,measurement="TBB_events",tags=tags,fields=fields)
db.push_events()
```