# Graphana 

## Dashboard
Pour visualiser tous ces événement on se sert de Graphana. 

![Dasboard Radiogaga](../figures/graphana_1.png)

Chaque **"paquet"** de points correspond à une observation. Si on zoom sur un paquet on peut voir avec plus de détail les différents points.
![Zoom sur une Observation](../figures/graphana_2.png)

## Courbe S/N ratio
Rapport S/N pour chaque polar, ainsi que le **udp_count** afin de retrouver l'événement

## Courge ALT et AZ
Permet de visualiser la corélation entre la direction pointé et la direction du résultat du **far field** 
!!! TODO 
    à améliorer : mettre à zéro les résultats incohérents   
    et non un résultat totalement aléatoire quand aucune zone chaude n'est détecté

## Tableau 
Permet d'extaire la liste en **.csv** des événement afficher sur l'échelle de temps choisis.
Cela permet d'injecter cette liste à un script python afin d'afficher les formes d'ondes et les différents traitements sur les événement de cette liste.

## Filtres
Graphana dispose d'un outil très puissant, c'est les filtres par **Tags**.  
Cela permet de sélectionner des événement très précisemment. 

![Filtres](figures/../../figures/graphana_filtre.png)
!!! TODO 
    Ajouter les filtres sur les **fields** en plus des **tags** (un peu plus compliqué)