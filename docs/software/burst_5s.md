# Burst 5s

## Introduction
Pour avoir une idée des formes d'ondes présentes sur les différents MR de NenuFar, nous avons jugé intéressant d'extraire 5 secondes de données temporelles sur l'ensemble des MR.  
POur réaliser cela nous allons utiliser les TBB. En effet chaque carte dispose 16Go de RAM, sur laquelle est écrite en temps réels les formes d'ondes des 8 canaux (4 * 2 polars). L'idée est donc de stopper l'écriture dans cette RAM sur l'ensemble des cartes exactement au même moment. (Très important !). Et ensuite venir les relire une à une tranquillement.  

## Trig simultané sur l'ensemble des carte

Actuellement il existait 2 mode pour stopper l'écrite des données dans la RAM.

  - **Le mode "ADC"** : qui consiste à injecter un pulse sur une entrée hardware de la carte. Ce mode aurait pû etre interessant pour réaliser le trig, mais il fallait cablé un cable sur chaque carte afin de déclancher manuellement (bouton, relai ? ). Cette méthode n'a donc pas été choisi pour des raisons de faisabilité.
  - **Le mode "Soft"** : qui consiste à écrire dans un registre, l'écriture fait office d'événement de trig. Cette méthode n'est pas adapté car il est impossible d'écrire exactement au même moment sur l'ensemble des cartes.

Pour résoudre ce problème Cedric a modifié le firmware de sorte à créer un nouveau mode.

``` vhdl
  -- Resync PPS_TRIGGER input on ASI_RX_CLK
  PPS_TRIGGER_r  <= PPS_TRIGGER   when rising_edge(ASI_RX_CLK);
  PPS_TRIGGER_rr <= PPS_TRIGGER_r when rising_edge(ASI_RX_CLK);
  -- Rising Edge on ADC_TRIGGER pin
  PPS_TRIGGER_rise <= PPS_TRIGGER_r and not PPS_TRIGGER_rr when rising_edge(ASI_RX_CLK);

  -- Trigger Source, validated by the Enable (no resync on TRIG_SRC_ENA)
  ASI_TRIG_REQ(TRIG_SRC_ADC) <= ADC_TRIGGER_rise and TRIG_SRC_ENA(TRIG_SRC_ADC);
  ASI_TRIG_REQ(TRIG_SRC_PPS) <= PPS_TRIGGER_rise and TRIG_SRC_ENA(TRIG_SRC_PPS);
```

  - **Le mode "PPS"** : Ce mode resemble un peu au mode **"Soft"** à l'exception prêt qu'une fois l'écriture faite dans le registre, l'écriture dans la RAM ne se stoppera qu'au prochain PPS. Il ne reste plus qu'a écrire dans ce registre entre 2 secondes rondes et le trig sera fait de manière parfaite sur l'ensemble des cartes.

## Envoi du trig

Comme vu ci-dessus il faut configurer le trig en mode en PPS et au prochain tick PPS, l'écriture des RAM sont stoppés.

Pour réaliser cela je me suis servi du programme de test [**/HPAPB/soft/lanewba_multi.c**](https://svn.obs-nancay.fr/svn/NenuFar/trunk/recepteur_LANewBa/trunk/firmware/HPAPB/soft/lanewba_multi.c)

Ce script se lance via la commande suivante :  

```
./lanewba_multi boards_test_backend.cfg tbb_trig_pps.cfg 
```
Ou si problème de librairie
```
LD_LIBRARY_PATH=. ./lanewba_multi boards_test_backend.cfg tbb_trig_pps.cfg 
```
Le programme prend 2 fichiers de configuration en paramètres 

Un fichier contenant la liste des cartes sur lesquelles on travaille.

=== "boards_test_backend.cfg"

    ```conf
    # IP address          register port      data port
    #192.168.3.155         2297               1297
    192.168.5.41           1531               1287
    192.168.5.43           1533               1289
    ```

Un fichier où l'on règle l'heure à laquelle on veut trigger.
=== "tbb_trig_pps.cfg"

    ```conf
    # Call the Burst_TBB() function at specified time (h m s)
    b 13 30 10
    # Exit program
    q
    ```

## Acquisation des données

Pour récupérer les données une fois les RAM stoppés, un soft avait été livré par ALSE, avec toute les fonctions adaptées. Ce programme ne gérait qu'une seule carte, il ne restait plus qu'à le modifier un peu afin de pouvoir gérer plusieurs cartes.

Pour ce faire je me suis pas mal inspiré de lanewb_multi.

Et j'ai donc créé  [**/HPAPB/soft/alse_tbb/tbb_multi.c**](https://svn.obs-nancay.fr/svn/NenuFar/trunk/recepteur_LANewBa/trunk/firmware/HPAPB/soft/alse_tbb/tbb_multi.c)

Il se lance de la même façon, mais avec un paramètre en plus, le nombre d'échantillon récupérés en hexadécimal (optionnel, totalité des 16.0 Go par défaut).

```
./tbb_multi boards_test_backend.cfg tbb_burst.cfg 0x800
```
Ou si problème de librairie
```
LD_LIBRARY_PATH=. ./tbb_multi boards_test_backend.cfg tbb_burst.cfg 0x800
```

=== "boards_test_backend.cfg"

    ```conf
    # IP address          register port      data port
    #192.168.3.155         2297               1297
    192.168.5.41           1531               1287
    192.168.5.43           1533               1289
    ```
  
=== "tbb_burst.cfg"

    ```conf
    # Network configuration
    n
    # Ask IRQ
    a
    # Quit Register acces thread
    q
    ```

Une fois lancé le programme va traiter les cartes une à une et générer des fichiers avec ce format **192.168.5.41_1639040705_dump.bin** de 16Go par carte.

-> [TEST report](../testsreports/burst_TBB.md)