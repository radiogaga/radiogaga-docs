# Cheat sheet
Those command should be launch form **nb0/tbb**, in **nenufarobs** user.  
Soft and files are in **HPAP/soft/** and **HPAP/soft/TBB/alse_tbb/**


## Backend de prod

### 1. Config boards (optional, launch if service is not running)

```bash
cd /home/tberthet/HPAPB/soft
sudo LD_LIBRARY_PATH=. ./lanewba_multi boards_prod_backend_10g.cfg full_config.cfg
```
### 2. Set time in **tbb_trig_pps.cfg**

```bash
cd /home/tberthet/HPAPB/soft
sudo nano tbb_trig_pps.cfg
```
### 3. Do the trig
```bash
cd /home/tberthet/HPAPB/soft
sudo LD_LIBRARY_PATH=. ./lanewba_multi boards_prod_backend_10g.cfg tbb_trig_pps.cfg
```

### 4. Read burst on TBB

Command to directly launch in  nb0:/data/**my_new_dump**/
```bash
# 16Go
sudo LD_LIBRARY_PATH=/home/tberthet/HPAPB/soft/TBB/alse_tbb/ /./home/tberthet/HPAPB/soft/TBB/alse_tbb/tbb_multi /home/tberthet/HPAPB/soft/boards_prod_backend_10g.cfg /home/tberthet/HPAPB/soft/TBB/alse_tbb/tbb_burst.cfg 0xFFF8000
# 512 Mo
sudo LD_LIBRARY_PATH=/home/tberthet/HPAPB/soft/TBB/alse_tbb/ /./home/tberthet/HPAPB/soft/TBB/alse_tbb/tbb_multi /home/tberthet/HPAPB/soft/boards_prod_backend_10g.cfg /home/tberthet/HPAPB/soft/TBB/alse_tbb/tbb_burst.cfg 0x80000

```
---
### 5. Transfer files to nancep

Command to launch from **nancep**.
```bash
# copy .bin from tbb to nancep
rsync -aP -e 'ssh -T -c aes128-gcm@openssh.com -o Compression=no' . tberthet@nancep6dt:/data/tberthet/
rsync -aP  tberthet@tbb:/data/my_dump/ .

```

### 6. Plot spectrograms

Command to launch from **nancep**.
The output **PDF** is generates in the folder with **.bin**
```bash
cd /home/tberthet/tbburst/
python3 plot_spectrogram.py /data/tberthet/my_dump/
```

