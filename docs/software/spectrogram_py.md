# plot_spectrogram.py
## Présentation
Afin de réaliser des temps fréquences des burst de 5s obtenu par les TBB. Ce script a été développé. 

On le lance avec la commande suivante : 

```
python3 plot_spectrogram.py /data/mon_dump/ 
```

Dans **mon_dump/** doit se trouver tous les .bin d'un unique et même dump.

Le script va générer un **pdf** avec un temps fréquences par polar de chaque MR.

![Temps fréquences généré](../figures/temps_freq.png)

Pour ne faire ressortir uniquement les événement intéressant, nous avons appliqué des filtres : 

- Pass band de 24-76MHz
- Réjecteur 73MHz
- Réjecteur 36Mhz

De plus nous avons retranché la valeur médiane des spectres afin d'avoir le temps fréquences "rapportés" à zero. Et faisant ressortir très clairement les événements.


## Optimisation du temps d'exécution

Ce traitement vu plus haut demande pas mal de puissance de calculs. Pour cela plusieurs test ont été fait et la meilleur option est de travail sur un **noeud nancep**, en parallélisant 16 calculs à la fois soit 2 fichiers.
Grace à ça le calcul met 5 min pour ces 2 fichiers. Donc un peut moins d'une heure pour les 21 fichiers.

On pourrait augmenter le nombre de coeurs, mais cela augmente considérablement la RAM utilisé, 16 coeurs est donc le meilleur compromis.

### Sur les fichiers binaires de base
#### 8 coeurs

Pour 8 coeurs, soit 1 coeurs par polar, le programme traite **1** fichier à la fois
```
real    9m0.320s
user    56m50.846s
sys     23m23.539s

```


#### 16 coeurs
!!! success "Meilleure option"
Pour 16 coeurs, soit 1 coeurs par polar, le programme traite **2** fichier à la fois  
```
real    5m32.417s
user    58m21.327s
sys     26m31.291s

```
### Sur un fichiers par polar 
J'ai essayé de parser les binaires en des fichiers de 2Go. Donc un fichier par polar, en espérant que cela faciliterai les accés d'écriture. Mais cela n'a rien changé, même plutot augmenté.
#### 8 coeurs
```
real    11m18.792s
user    53m0.208s
sys     20m22.131s
```
