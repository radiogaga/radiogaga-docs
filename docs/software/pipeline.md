# Pipeline

!!! info
    Depuis le 05/12/2022  
    Le pipeline a été porté sur la machine **codadaqsa2**.  

## Principe

![Pipeline](../figures/pipeline_radiogaga.jpg)

Les machines :

- **radiogaga** : soft de contrôle/commande **@ctaffoureau**
- **NB0** : soft tbb_eparse, acquisition des TBB>zip>transfert
- **codadasa2** : stockage des événements / pipeline

## Fonctionnement

Voici le script de pipeline : 

[https://gitlab.obspm.fr/radiogaga/radiogaga-python/-/blob/main/scripts/services/watchers.py](https://gitlab.obspm.fr/radiogaga/radiogaga-python/-/blob/main/scripts/services/watchers.py)

Il tourne dans un service nommé **radiogaga**
 ``` bash
 service radiogaga stop

 service radiogaga start
 ```

### Mode temps réél

La nouveauté de ce pipeline est qu'il traite les événements en temps réel. En effet dés que de nouveaux événements sont placés dans le répertoire tampon journalier, le pipeline les copie et les traite à la volé.  
Lors de l'obs, les événements sont justes re-procéssés et les données (du process mais aussi de la trame) sont enregistrés dans la **base influxDB**.

### Fin de l'obs

Une fois l'observation terminée, plusieurs chose se passent.

#### Timeout

Un **timeout** se met en place. En effet les zips sont copiés toutes les **minutes** par paquet de **60**. Ils peut arriver que NBO soit "endétées d'événements", cad. qu'il en trig plus qu'il n'en transfert.  
Donc ce timeout se remet à zéro tant que des événements sont reçus sur **codadadqsa2**.

#### Post processing

Une fois le timeout écoulé, on va venir chercher tous les événements de l'ob courante dans la base donnée. Faire 2,3 stats et sélectionner les meilleurs événements : 

``` python 
## Query to select "positive" events
degree = 5
heatmap_ratio = 1
# Comment your query to print into the resume for the mail 
commented_query = f"{degree} degree around alt/az pointing and heatmap_ratio sould be >= to {heatmap_ratio}"
### your query
query  = f'alt_event > (alt_source - {degree})'
query += f' and alt_event < (alt_source + {degree})'
query += f' and  az_event > az_source - {degree}'
query += f' and az_event < az_source + {degree}'
query += f' and heatmap_ratio > {heatmap_ratio}'
df_selected = df.query(query)
df_selected['name'].to_csv(f'{HOME_DIR}/pipeline_selected.txt') 
n_selected_events = len(df_selected)

```

Enfin on fait un PDF avec ces événements, et c'est terminée, le pipeline est pret pour une nouvelle OBS.    
Si des observations se suivent et que le pipeline n'est pas ready c'est pas grave. Le fichier **obs_to_process.txt** sert de file d'attente.


#### Mail de recap

Pour résumer tout ça un mail est envoyé. 

![Mail](../figures/mail.png)

