
# Adresses des différents registre

Pour le soft toutes les adresses se trouvent dans [svn/NenuFar/trunk/recepteur_LANewBa/trunk/firmware/HPAPB/src/PROCESSING_FOR_RADIOGAGA/src/python/map_address/RSP_constants.h](https://svn.obs-nancay.fr/svn/NenuFar/trunk/recepteur_LANewBa/trunk/firmware/HPAPB/src/PROCESSING_FOR_RADIOGAGA/src/python/map_address/RSP_constants.h)

Les addresses globales de la cartes sont exactement les mêmes que pour LANewBa (ADC, DSD, SST etc.)

Pour écrire dans les différents registre il faut "additionner" les adresses. 
Par exemple pour écrire dans le registre **K** du trigger il faut écrire à l'addresse :   

c_processing_address_offset +  
c_radiogaga_address_offset +  
c_trigger_address_offset +  
c_trigger_k_offset  

= **0x13A0000**

Pour plus de détail à propos des registres de chaque module cf. **Firmware/Signal Processing/**

=== "RSP_constants.h"
    ``` c
    #define c_processing_address_offset         (  16973824)  // AVM_GDK_PROCESSING_BASE + 16#0003_0000#; ####  Computed from: 16#0103_0000#

    // A partir de la ligne 859
    #define c_radiogaga_address_offset          (   3211264)  // 134 - c_processing_address_offset ####  Computed from: 16#0031_0000#


    #define c_ipd_address_offset                (         0)  //  ####  Computed from: 16#0000_0000#
    #define c_ipd_delay_x_offset                (         0)  //  ####  Computed from: 16#00#
    #define c_ipd_delay_y_offset                (        16)  //  ####  Computed from: 16#10#


    #define c_ipd_mem_depth                     (       512)  //  ####  Computed from: 512


    #define c_dcb_address_offset                (     65536)  //  ####  Computed from: 16#0001_0000#
    #define c_dcb_biais_x_offset                (         0)  //  ####  Computed from: 16#00#
    #define c_dcb_biais_y_offset                (        16)  //  ####  Computed from: 16#10#
    #define c_dcb_accu_x_low_offset             (        32)  // lower 32 bits of accumulator ####  Computed from: 16#20#
    #define c_dcb_accu_y_low_offset             (        48)  // lower 32 bits of accumulator ####  Computed from: 16#30#
    #define c_dcb_accu_x_high_offset            (        64)  // higher 8 bits of accumulator ####  Computed from: 16#40#
    #define c_dcb_accu_y_high_offset            (        80)  // higher 8 bits of accumulator ####  Computed from: 16#50#


    #define c_presum_address_offset             (    131072)  //  ####  Computed from: 16#0002_0000#
    #define c_presum_ant_sel_x_offset           (         0)  //  ####  Computed from: 16#00#
    #define c_presum_ant_sel_y_offset           (        16)  //  ####  Computed from: 16#10#
    #define c_presum_rampe_en_offset            (        32)  //  ####  Computed from: 16#20#


    #define c_filter_address_offset             (    196608)  // 3 instanciation of filter ####  Computed from: 16#0003_0000#
    #define c_filter_coef_offset                (         0)  //  ####  Computed from: 16#00#
    #define c_filter_shift_offset               (       255)  //  ####  Computed from: 16#FF#


    #define c_trigger_address_offset            (    393216)  //  ####  Computed from: 16#0006_0000#
    #define c_trigger_k_offset                  (         0)  //  ####  Computed from: 16#00#
    #define c_trigger_c_num_offset              (        16)  //  ####  Computed from: 16#10#
    #define c_trigger_c_den_offset              (        32)  //  ####  Computed from: 16#20#
    #define c_trigger_rising_time_min_offset    (        48)  //  ####  Computed from: 16#30#
    #define c_trigger_rising_time_max_offset    (        64)  //  ####  Computed from: 16#40#
    #define c_trigger_trig_time_max_offset      (        80)  //  ####  Computed from: 16#50#

    // Addresse des IST
    #define c_BLP_radiogaga_address_offset      (    458752)  //  ####  Computed from: 16#0007_0000#
    ```