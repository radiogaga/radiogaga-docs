# Le principe
Donc sur une somme de MRs phasées>sommées>filtrées on se retrouve avec les 2 polarisations X et Y. Le trigger tourne en parallèle sur chacune des polars.  
Donc pour chacun de ces 2 signaux voici la logique de détection : 

![Principe](../../figures/trigger_principe.png)

#### Plot 1 

On voit nos 2 signaux ayant suivi tout le traitement de signal.

#### Plot 2 

Ici on vient faire un puissance accumulé glissante sur 128 échantillons. Comme ceci : 
``` vhdl
acc_pow(idx + 1) <= acc_pow(idx) + signed(in_data.data(g_in_data_w - 1 downto 0)) * signed(in_data.data(g_in_data_w - 1 downto 0));
```

soit :  

$$
\sum_{i=0}^{128} out(i)^2 / 128
$$

Ce signal va servir à plusieurs choses:

- Calculer la dérivé
- Calculer le temps de montée
- Estimer un plancher de bruit à ne pas dépasser

#### Plot 3 

Enfin sur la puissance on va venir faire une derivé instantanée par rapport à l'échantillon **n-8**.  
C'est sur cette dérivé qu'on va venir mettre un seuil **K** et lancer la [machine d'état](state_machine.md). 
