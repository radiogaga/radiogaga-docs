# Cahiers des charges

Le cahiers des charges avait 2 principaux points sur lequel validé ou rejeter un événement :
### Etre isolé 
![Fenêtre avant/aprés](../../figures/cdc_0.png)

### Avoir un temps de monté très court
![Temps de monté](../../figures/cdc_1.png)