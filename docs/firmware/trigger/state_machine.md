# Machine d'état

Le **VHDL** étant un language temps réel. Il a fallu ruser afin de pouvoir faire tous les calculs nécessaire à la validation d'un événement.  
Pour cela on utilise une machine d'état qui va faires les étapes de validation en plusieurs étapes.  
Voici un schéma généré par un outil (pas forcément très explicite) :

![Machine d'état](../../figures/state_machine_trigger.png)

## Plus en détail 

#### s_idle
Etat d'attente, **-> s_eval_max** quand la dérivé dépasse le seuil *K* **ET** que le niveau de bruit de la puissance est en dessous de la valeur paramétré.

#### s_eval_max
Ici on va venir détecter le maximum de la puissance accumulé glissante, pour cela on fait comme ça : 

``` vhdl
--------------------------------------------------------------------------------------------------------------------
--s_eval_max
-------------------------------------------------------------------------------------------------------------------- 
when s_eval_max =>
  -- Evaluation du maximum et de m20 et m80 et m50
  if pow > max then
    max <= std_logic_vector(unsigned(pow)) ;
    max_without_noise <= std_logic_vector(unsigned(pow) - level_bruit) ;
  end if;

  -- Regarde si on entame la descente
  if derive <= shift_right(signed(parameters.k1), 1) * (-1) then
    rising    <= '0';
    --max <= (others => '0'); 
    max_20 <= std_logic_vector(resize(shift_right(unsigned(max_without_noise) * parameters.pourcent_20 , 4) , 25) + level_bruit);  -- 100/16*2 =  12.5%
    --max_50 <= std_logic_vector(resize(shift_right(max * 4 , 4) , 25)  );  -- 100/16*4 =  25% / pas utilisé
    max_80 <= std_logic_vector(resize(shift_right(unsigned(max_without_noise) * parameters.pourcent_80 , 4) , 25) + level_bruit);  -- 100/16*11 =  68.75  
    timer <= 20; -- set timer 
    state <= s_waiting_max20;
  end if;
```

On vient suivre la pente pour obtenir un maximum, une fois qu'on entame la descente (dérivé négative) on valide le maximum et on calcul la valeur des paliers qui vont servir pour calculer le **temps de monté** (par exemple: 20-80% du maximum, configurable via l'interface registre).
??? info
    On vient retirer la valeur du bruit moyen, afin de ne pas fausser le calcul du temps de monté


#### s_waiting_max20
Cet état attend de bien redescendre en dessous du premier paliers(par exemple :20%). Ceci permet d'être sur d'avoir redescendu la pente. 

#### s_cnt_rising
Une fois nos 2 paliers du maximum de la pente détecter (20-80%). On va venir reparcourir la pente de la puissance accumulé glissante et incrémenter un compteur quand cette valeur est comprise entre ces 2 paliers. Comme ceci : 

``` vhdl
-- Comptage du temps entre m20 et m80
if p_pow(g_window_w + 20) >= max_20 and p_pow(g_window_w + 20) <= max_80 then
  cnt   <= cnt + 1;
-- Si on est supérieur à m80 on arrete
elsif p_pow(g_window_w + 20) > max_80 and cnt >= 1 then
    cnt_security <= parameters.window_w;
    state <= s_security;
end if;
-----------
```
#### s_security
Enfin le dernier état avant la validation de l'événement, on va venir patienter **128** échantillons (réglable), vérifier qu'aucun autre événement impulsifs n'est pas détecter.   
Pour cela c'est la même condition que **s_idle**, K > dérivé.   
A la fin de ce compteurs et après avoir vérifié si le **temps de monté** rentre dans les tolérances indiqués (en échantillon), si oui on passe à l'état **s_pulse**.  
Sinon si l'événement est rejeté on retourne à **s_idle**.


#### s_pulse
Dernier état on vient juste généré un signal qui va venir déclancher l'envoi de la trame via [UDP_sender](../UDP_sender.md).
??? info
    On vient généré un signal à **'1'** sur 5 échantillons. Ensuite on fera un **OU** entre ce signal des deux polars. Afin de prendre l'un ou l'autre.  
    Le fait d'avoir un signal de 5 échantillons est pour éviter d'avoir 2 déclenchement si les 2 polars ne sont parfaitement en phase. 

#### s_limit
On rentre dans cet état lorsque qu'un deuxième pulse est détecté, pour cela dans chaque état se trouve les lignes suivantes : 
```vhdl
-- Securite pour eviter les pulses mutiples 
if derive >= signed(parameters.k1) then
  limit <= parameters.window_w;
  state <= s_limit;
  cnt_ko <= std_logic_vector(unsigned(cnt_ko)+1);
end if;
```
Une fois dans l'état **s_limit** c'est un peu le même principe que **s_security**, on va attendre **128** échantillons et retourner à s_idle.   
Cet état remplis la fonction de **fenêtre** avant et après. 