# Transit sur CygA
!!! success

Suite au succès du test sur diode à bruit, il fallait faire un test sur ciel. Pour cela nous avons réalisé un transit sur CygA avec 8 MR qui sont les suivants : 

| carte 20   | carte 21  |
|------------|-----------|
| 3 MR70     | 3 MR79    |
| 2 MR68     | 2 MR78    |
| 1 MR66     | 1 MR75    |
| 0 MR64     | 0 MR72    |

![Spectre](../figures/cygA.png)
![Coupe 40MHz](../figures/cygA_coupe40M.png)

On arrive bien à distinger les différents lobes. Le résultat dépend énormément des MR choisis. 