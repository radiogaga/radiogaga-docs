# Test du module monitor après identification d'un problème de lecture via le MAX 10
> 2022/05/13  
> Version firmware : 2021.12.01  
> T.BERTHET, C. VIOU

## Test réalisé
### BACKEND de TEST
Le test a été réalisé sur le backend de TEST avec les cartes dans l'ordre de placement : 0,20,16,24,26 soit les IP et ports suivants. 
```
[0] IP=192.168.3.128 ; REG_PORT=0x08DF ; DATA_PORT = 0x04F7 
[1] IP=192.168.3.148 ; REG_PORT=0x08F3 ; DATA_PORT = 0x050B
[2] IP=192.168.3.144 ; REG_PORT=0x08EF ; DATA_PORT = 0x0507
[3] IP=192.168.3.152 ; REG_PORT=0x08F7 ; DATA_PORT = 0x050F
[4] IP=192.168.3.154 ; REG_PORT=0x08F9 ; DATA_PORT = 0x0511
```

Pour réaliser ce test le soft de test **lanewba_multi** a été utilisé. On a passé la liste des cartes ci dessus et le fichier de commande **test_init_monitor.conf** ci-joint pour initialiser les cartes et le module monitor.
```
./lanewba_multi boards_test_backend.cfg test_init_monitor.conf 
```
Ensuite on relance la même commande avec le fichier **test_read_monitor.conf**, afin de lire la plage d'adresse registre de monitor.
```
./lanewba_multi boards_test_backend.cfg test_read_monitor.conf
```

## Constat
En sortie on retrouve 5 fichiers logs générés par le soft **lanewba_multi**, avec comme nom "cmd_**port**".log on peut associer fichier/cartes avec la liste ci-dessus.  
Le problème est que sur les cartes 0,16,24,26 on lit **FF** dans la plage d'adresse de **0x020300** à **0x2030B**. Uniquement la carte **20** renvoie des valeurs cohérentes et fluctuantes.

Le détail des cartes avec date, version,résultat etc. se trouve dans inventory.md.

---
## BACKEND de PROD 
Même test réalisé sur quelques cartes en production toutes les relectures sont OK. 
```
[0] IP=192.168.5.5 ; REG_PORT=0x05D7 ; DATA_PORT = 0x04E1, CONFIG_FILE 
[1] IP=192.168.5.19 ; REG_PORT=0x05E5 ; DATA_PORT = 0x04E8, CONFIG_FILE 
[2] IP=192.168.5.31 ; REG_PORT=0x05F1 ; DATA_PORT = 0x04EE, CONFIG_FILE 
[3] IP=192.168.5.37 ; REG_PORT=0x05F7 ; DATA_PORT = 0x04F1, CONFIG_FILE 
[4] IP=192.168.5.55 ; REG_PORT=0x0609 ; DATA_PORT = 0x04F3, CONFIG_FILE 
```

Les fichiers de conf et les sorties se trouve dans [ici](https://svn.obs-nancay.fr/svn/NenuFar/trunk/receivers/RadioGAGA/doc/docs/testsreports/test_monitor)