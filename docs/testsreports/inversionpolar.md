# Inversion Polarisation
Un signal PPS a été distrubé sur le MR70 entrée de splitter entre le backend de prod et le backend de test utilisé pour Radiogaga. Voici  3 différentes vues :
 

### Trig TBB eparse vue en tant que polar X sur le backend de prod

![pps_tbb_eparse.png](../figures/pps_tbb_eparse.png)

### Vue en polar Y dans signal Tap sur le backend de test

![pps_stp.png](../figures/pps_stp.png)

### Vue en polar NE dans les SST  sur la VCR

![pps_sst.png](../figures/pps_sst.png)

!!! TODO
    Vérifier sur un dump