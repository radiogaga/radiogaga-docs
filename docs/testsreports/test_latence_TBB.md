# Fix bug latence
Nous avions un problème sur le placement de l'événement dans la fenetre TBB. Il y'avait un delta de 2560 ns.  
Le problème venait de la façon dont la nanosecondes était calculé. Elle était remise à zéro sur un changement de timestamp, mais ce dernier ne change pas au même moment sur les secondes paires ou impaire. 

![Passage paire/impaire](../figures/secondes_paire.png)
![Passage impaire/paire](../figures/secondes_impaire.png)
