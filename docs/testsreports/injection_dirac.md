# Test dirac injecté sur les cartes via un GBF

## Caractéristique du signal :  
Amplitude 100mV  
Période : 10 secondes  
Tps montée : 5ns  
Durée : 10ns  

![Dirac vu à l'oscilloscope](figures/../../figures/dirac_gbf.jpg)

# Coté FPGA 

![Différents signaux du FPGA dans signal TAP](figures/../../figures/dirac_fpga.png)

La valeur lu en entrée ADC est de 530. On peut donc conclure que 1mV fait environ 5.3 unité ADC.

