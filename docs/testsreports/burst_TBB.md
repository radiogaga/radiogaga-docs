# TBB Burst
!!! success

## Backend de test
Pour tester le soft réalisé. Nous avons utilisés 2 cartes du backend de test.  
Sur les polars X des 2 cartes est branché une diode à bruit et sur les polar Y des MR.  
Donc on lance le programme vu dans la documentation du mode [Burst 5s](../software/burst_5s.md)

Et on obtient 2 fichiers suivants : 
```
Nom                                 Taille
192.168.5.41_1639038895_dump.bin    16.0 Go    
192.168.5.43_1639038895_dump.bin    16.0 Go
```
Pour lire ces fichiers on se sert de [**/HPAPB/soft/alse_tbb/load_bin.py**](https://svn.obs-nancay.fr/svn/NenuFar/trunk/recepteur_LANewBa/trunk/firmware/HPAPB/soft/alse_tbb/load_bin.py)

On peut voir 2 plots intéressant, le spectre de la phase de cross-corrélation et un spectre classique.  
Le premier va nous permettre de vérifier si l'on a pas de déphasage entre les différentes voies et/ou cartes.
Et le second permet de vérifier si l'on retrouve bien la forme du ciel et de la diode à bruit en fréquence.
Afficher les forme d'ondes n'a pas de réél intéret surtout que ce sont des plots très lourd à afficher vu du nombre de points.

![Spectre en phase de cross-corrélation](../figures/Corrélation-2cartes.png)

La légère déviation est probablement dû à la longueur des câbles qui distribue le signal PPS d'une par rapport à l'autre.

![Spectre en amplitude](../figures/spectre%202%20cartes.png)

## Backend de prod
Au préalable il a fallu bien configurer les longueurs de câbles à **zero** dans le module **DSD**.

![Spectre en phase de cross-corrélation](../figures/test_synchro_prod_zoomed.png)

Le resultat est quasimenet parfait car les câbles distrubuant le PPS font tous exactement la même taille contrairement au backend de test où il s'agit de rallonges faites avec des **i**.

![Spectre en amplitude 1](../figures/test_synchro_prod_spectre.png)
![Spectre en amplitude 2](../figures/test_synchro_prod_spectre_2.png)

Nous avons fait 2 dumps différents pour s'assurer que le bruit généré par la diode à bruit était bien aléatoire et que la pattern visualisé était bien différent à chaque fois.