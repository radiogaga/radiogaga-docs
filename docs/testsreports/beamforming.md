# Beamforming sur diode à bruit
!!! success

L'objectif de ce test est de balayé le ciel avec une multitude de pointage sur un axe NS.
![Vue 3D](../figures/beamforming_3d.png)
Pour les entrées nous ne regardons pas le ciel mais une diode à bruit, que nous avons branché sur chacune des polard X de 2 cartes soit 8 voies au total.  



## Configuration

Une fois le câblage fait il faut modifier la position des MR associé à chaque voie afin de les placer tous les 10m de façon aligné sur l'axe NS.

Pour cela on modifie le fichier suivant dans le dataNCU :
```
MR_Ant10_position.conf.bak

64 0.0 0.0 181.486 4323991.578 165400.547 4670298.769
65 0.0 0.0 181.296 4324019.342 165340.125 4670275.102
66 0.0 10.0 181.236 4324041.969 165298.798 4670255.665
67 0.0 0.0 181.772 4324031.550 165447.180 4670260.756
68 0.0 20.0 181.764 4324069.128 165430.412 4670226.776
69 0.0 0.0 181.178 4324073.658 165382.196 4670223.510
70 0.0 30.0 181.232 4324097.712 165405.375 4670200.645
71 0.0 0.0 181.782 4324088.757 165467.735 4670207.435
72 0.0 40.0 181.801 4324026.758 165474.088 4670264.256
73 0.0 0.0 181.772 4324046.044 165482.279 4670246.191
74 0.0 0.0 182.587 4324063.248 165560.571 4670228.721
75 0.0 50.0 181.792 4324087.361 165522.565 4670206.803
76 0.0 0.0 182.605 4324090.078 165600.750 4670202.655
77 0.0 0.0 181.881 4324121.657 165528.288 4670175.180
78 0.0 60.0 182.602 4324045.962 165592.592 4670243.510
79 0.0 70.0 182.415 4324123.151 165640.388 4670170.583
```

Les numéros des MR correspondents à des MR qui été déjà associé à chacune des voies.

## Génération de l'observation
Une fois cela fait on peut générer une observation avec le script 
[linear_array_simulated_beamforming.py](https://svn.obs-nancay.fr/svn/NenuFar/trunk/recepteur_LANewBa/trunk/firmware/HPAPB/soft/BST_two_boards/BST_SST_tests/linear_array_simulated_beamforming.py)

## Résultat 

Une fois l'observation terminée, on peut ouvrir le fichier IST.fits afin de vérifier le beamforming.

![Beamforming](../figures/beamforming.png)

Ce test avait déjà été fait pour lanewba, le pattern obtenu est celui attendu. 


